FROM golang:1.9.2
WORKDIR /
ADD main.go index.html aviator.png /
RUN go get -d -v .
RUN go build .
CMD ["/aviator"]
